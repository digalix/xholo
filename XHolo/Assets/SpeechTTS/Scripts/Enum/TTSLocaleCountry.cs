﻿using UnityEngine;
using System.Collections;

public enum TTSLocaleCountry{
	AFGHANISTAN,
	ALAND_ISLANDS,
	ALBANIA,
	ALDERNEY,
	ALGERIA,
	AMERICAN_SAMOA,
	ANDORRA,
	ANGOLA,
	ANGUILLA,
	ANTARCTICA,
	ANTIGUA_AND_BARBUDA,
	ARGENTINA,
	ARMENIA,
	ARUBA,
	ASCENSION_ISLAND,
	AUSTRALIA,
	AUSTRIA,
	AZERBAIJAN,
	BAHAMAS,
	BAHRAIN,
	BANGLADESH,
	BARBADOS,
	BELARUS,
	BELGIUM,
	BELIZE,
	BENIN,
	BERMUDA,
	BHUTAN,
	BOLIVIA,
	BONAIRE_ST_EUSTATIUS_AND_SABA,
	BOSNIA_AND_HERZEGOVINA,
	BOTSWANA,
	BOUVET_ISLAND,
	BRAZIL,
	BRITISH_INDIAN_OCEAN_TERRITORY,
	BRUNEI_DARUSSALAM,
	BULGARIA,
	BURKINA_FASO,
	BURUNDI,
	CAMBODIA,
	CAMEROON,
	CANADA,
	CAPE_VERDE,
	CAYMAN_ISLANDS,
	CENTRAL_AFRICAN_REPUBLIC,
	CHAD,
	CHANNEL_ISLANDS,
	CHILE,
	CHINA,
	CHRISTMAS_ISLAND,
	COCOS_ISLANDS,
	COLOMBIA,
	COMOROS,
	CONGO_REPUBLIC_OF,
	CONGO,_THE_DEMOCRATIC_REPUBLIC_OF_THE_formerly_Zaire,
	COOK_ISLANDS,
	COSTA_RICA,
	COTE_DIVOIRE,
	CROATIA,
	CUBA,
	CURACAO,
	CYPRUS,
	CZECH_REPUBLIC,
	DENMARK,
	DJIBOUTI,
	DOMINICA,
	DOMINICAN_REPUBLIC,
	ECUADOR,
	EGYPT,
	EL_SALVADOR,
	EQUATORIAL_GUINEA,
	ERITREA,
	ESTONIA,
	ETHIOPIA,
	FAEROE_ISLANDS,
	FALKLAND_ISLANDS,
	FIJI,
	FINLAND,
	FRANCE,
	FRENCH_GUIANA,
	FRENCH_POLYNESIA,
	FRENCH_SOUTHERN_TERRITORIES,
	GABON,
	GAMBIA_THE,
	GEORGIA,
	GERMANY,
	GHANA,
	GIBRALTAR,
	GREAT_BRITAIN,
	GREECE,
	GREENLAND,
	GRENADA,
	GUADELOUPE,
	GUAM,
	GUATEMALA,
	GUERNSEY,
	GUINEA,
	GUINEA_BISSAU,
	GUYANA,
	HAITI,
	HEARD_ISLAND_AND_MCDONALD_ISLANDS,
	HONDURAS,
	HONGKONG,
	HUNGARY,
	ICELAND,
	INDIA,
	INDONESIA,
	INTERNATIONAL_ORGANIZATIONS,
	IRAN,
	IRAQ,
	IRELAND,
	ISLE_OF_MAN,
	ISRAEL,
	ITALY,
	JAMAICA,
	JAPAN,
	JERSEY,
	JORDAN,
	KAZAKHSTAN,
	KENYA,
	KIRIBATI,
	NORTH_KOREA,
	SOUTH_KOREA,
	KUWAIT,
	KYRGYZSTAN,
	LAO_PEOPLES_DEMOCRATIC_REPUBLIC,
	LATVIA,
	LEBANON,
	LESOTHO,
	LIBERIA,
	LIBYA,
	LIECHTENSTEIN,
	LITHUANIA,
	LUXEMBOURG,
	MACAO,
	MACEDONIA,
	MADAGASCAR,
	MALAWI,
	MALAYSIA,
	MALDIVES,
	MALI,
	MALTA,
	MARSHALL_ISLANDS,
	MARTINIQUE,
	MAURITANIA,
	MAURITIUS,
	MAYOTTE,
	MEXICO,
	MICRONESIA,
	MOLDOVA,
	MONACO,
	MONGOLIA,
	MONTENEGRO,
	MONTSERRAT,
	MOROCCO,
	MOZAMBIQUE,
	MYANMAR,
	NAMIBIA,
	NAURU,
	NEPAL,
	NETHERLANDS,
	NETHERLANDS_ANTILLES,
	NEW_CALEDONIA,
	NEW_ZEALAND,
	NICARAGUA,
	NIGER,
	NIGERIA,
	NIUE,
	NORFOLK_ISLAND,
	NORTHERN_MARIANA_ISLANDS,
	NORWAY,
	OMAN,
	PAKISTAN,
	PALAU,
	PALESTINIAN_TERRITORIES,
	PANAMA,
	PAPUA_NEW_GUINEA,
	PARAGUAY,
	PERU,
	PHILIPPINES,
	PITCAIRN,
	POLAND,
	PORTUGAL,
	PUERTO_RICO,
	QATAR,
	REUNION,
	ROMANIA,
	RUSSIAN_FEDERATION,
	RWANDA,
	SAINT_BARTHELEMY,
	SAINT_HELENA,
	SAINT_KITTS_AND_NEVIS,
	SAINT_LUCIA,
	SAINT_MARTIN,
	SAINT_PIERRE_AND_MIQUELON,
	SAINT_VINCENT_AND_THE_GRENADINES,
	SAMOA,
	SAN_MARINO,
	SAO_TOME_AND_PRINCIPE,
	SAUDI_ARABIA,
	SENEGAL,
	SERBIA,
	SEYCHELLES,
	SIERRA_LEONE,
	SINGAPORE,
	SINT_MAARTEN,
	SLOVAKIA,
	SLOVENIA,
	SOLOMON_ISLANDS,
	SOMALIA,
	SOUTH_AFRICA,
	SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS,
	SOUTH_SUDAN,
	SPAIN,
	SRI_LANKA,
	SUDAN,
	SURINAME,
	SVALBARD_AND_JAN_MAYEN,
	SWAZILAND,
	SWEDEN,
	SWITZERLAND,
	SYRIAN_ARAB_REPUBLIC,
	TAIWAN,
	TAJIKISTAN,
	TANGANYIKA,
	TANZANIA,
	THAILAND,
	TIMOR_LESTE,
	TOGO,
	TOKELAU,
	TONGA,
	TRINIDAD_AND_TOBAGO,
	TUNISIA,
	TURKEY,
	TURKMENISTAN,
	TURKS_AND_CAICOS_ISLANDS,
	TUVALU,
	UGANDA,
	UKRAINE,
	UNITED_ARAB_EMIRATES,
	UNITED_KINGDOM,
	UNITED_STATES,
	UNITED_STATES_MINOR_OUTLYING_ISLANDS,
	URUGUAY,
	UZBEKISTAN,
	VANUATU,
	VATICAN_CITY,
	VENEZUELA,
	VIETNAM,
	VIRGIN_ISLANDS_BRITISH,
	VIRGIN_ISLANDS_US,
	YUGOSLAVIA,
	WALLIS_AND_FUTUNA,
	WESTERN_SAHARA,
	YEMEN,
	ZAMBIA,
	ZANZIBAR,
	ZIMBABWE
}
