﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Digalix.Dictionary
{
    public class ENDictionary : XHoloDictionary
    {
        
        public override void StartDictionary()
        {
            AnswerWords = new Dictionary<string, string>();
            
            #region Answers
            AnswerWords.Add(DictionaryIndex.A_DontUnderstand, "Sorry, I don't understand you");
            AnswerWords.Add(DictionaryIndex.A_TryAgain, "Please, try again");
            AnswerWords.Add(DictionaryIndex.A_StateMainMenu, "Ok, You're on MainMenu");
            AnswerWords.Add(DictionaryIndex.A_QuestionHours, "What time you want to wake you up ?");
            AnswerWords.Add(DictionaryIndex.A_WeareinMainMenu, "Ok, we are back on the main menu");
            AnswerWords.Add(DictionaryIndex.A_RepeatAlarm, "Ok, I will repeat the alarm every day, now we are on the main menu again");
            AnswerWords.Add(DictionaryIndex.A_AlarmOnlyToday, "Ok, I will set the alarm only for today, now we are on the main menu again");
            AnswerWords.Add(DictionaryIndex.A_SaveConfiguration, "Ok, I will save your configuration, would you like to repeat the alarm?");
            AnswerWords.Add(DictionaryIndex.A_BackToAlarmClock, "We're back on the alarm clock, ");
            AnswerWords.Add(DictionaryIndex.A_Understand, "I've understand");
            AnswerWords.Add(DictionaryIndex.A_Oclock, "o clock, is this correct?");
            AnswerWords.Add(DictionaryIndex.A_UnavailableHour, "this is an unavailable hour, please set a correct hour, ");
            AnswerWords.Add(DictionaryIndex.A_HoursAnd, "hours and");
            AnswerWords.Add(DictionaryIndex.A_AndMinutes, "minutes, is this correct ?");
            #endregion

            #region Requests
            AnswerWords.Add(DictionaryIndex.R_Dance, "\"dance\"");
            AnswerWords.Add(DictionaryIndex.R_HyLola, "\"hilola\"");
            AnswerWords.Add(DictionaryIndex.R_Exit, "\"exit\"");
            AnswerWords.Add(DictionaryIndex.R_Next, "\"next\"");
            AnswerWords.Add(DictionaryIndex.R_Prev, "\"previous\"");
            AnswerWords.Add(DictionaryIndex.R_Person, "\"wisdom.person\"");
            AnswerWords.Add(DictionaryIndex.R_Weather, "\"weather.search\"");
            AnswerWords.Add(DictionaryIndex.R_Joke, "\"joke\"");
            AnswerWords.Add(DictionaryIndex.R_SmallTalk, "\"smalltalk.person\"");
            AnswerWords.Add(DictionaryIndex.R_AlarmClock, "\"alarmclock\"");
            AnswerWords.Add(DictionaryIndex.R_HoursAndMinutes , "\"hoursminutes\"");
            AnswerWords.Add(DictionaryIndex.R_HoursOclock, "\"hoursoclock\"");
            AnswerWords.Add(DictionaryIndex.R_Confirmation, "\"yes\"");
            AnswerWords.Add(DictionaryIndex.R_Negation, "\"no\"");
            AnswerWords.Add(DictionaryIndex.R_Avatar, "\"avatar\"");
            #endregion

        }
        
    }
}
