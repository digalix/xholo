﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Digalix.Dictionary
{

    public abstract class XHoloDictionary 
    {

        public static Dictionary<string, string> AnswerWords;
       
            public virtual Dictionary<string, string> answerwords
        {
            set
            {
                value = answerwords;
            }
            get
            {
                return AnswerWords;
            }
        }

        public virtual void StartDictionary()
        {

        }
       
    }
}
