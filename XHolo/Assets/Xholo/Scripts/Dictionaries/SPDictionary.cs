﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Digalix.Dictionary
{
    public class SPDictionary : XHoloDictionary
    {
        
        public override void StartDictionary()
        {
            AnswerWords = new Dictionary<string, string>();

            #region Answers
            AnswerWords.Add(DictionaryIndex.A_DontUnderstand, "Lo siento, no te he entendido");
            AnswerWords.Add(DictionaryIndex.A_TryAgain, "Vuelve a intentarlo, por favor");
            AnswerWords.Add(DictionaryIndex.A_StateMainMenu, "Vale, estás en el menú principal");
            AnswerWords.Add(DictionaryIndex.A_QuestionHours, "A que hora quieres que te despierte ?");
            AnswerWords.Add(DictionaryIndex.A_WeareinMainMenu, "De auerdo, estamos en el menú principal");
            AnswerWords.Add(DictionaryIndex.A_RepeatAlarm, "De acuerdo, voy a repetir la alrma cada dia, ahora volvamos al menu principal");
            AnswerWords.Add(DictionaryIndex.A_AlarmOnlyToday, "De acuerdo, voy a poner la alarma solo para hoy, ahora volvamos al menu principal");
            AnswerWords.Add(DictionaryIndex.A_SaveConfiguration, "Bien, Voy a guardar la configuracion, quieres que repita la alarma?");
            AnswerWords.Add(DictionaryIndex.A_BackToAlarmClock, "Hemos vuelto a la configuracion de la alarma, ");
            AnswerWords.Add(DictionaryIndex.A_Understand, "He entendido");
            AnswerWords.Add(DictionaryIndex.A_Oclock, "en punto, es correcto?");
            AnswerWords.Add(DictionaryIndex.A_UnavailableHour, "Esta hora no es posible, por favor dime una hora correcta, ");
            AnswerWords.Add(DictionaryIndex.A_HoursAnd, "horas y");
            AnswerWords.Add(DictionaryIndex.A_AndMinutes, "minutos, es correcto ?");
            #endregion

            #region Requests
            AnswerWords.Add(DictionaryIndex.R_Dance, "\"baila\"");
            AnswerWords.Add(DictionaryIndex.R_HyLola, "\"holalola\"");
            AnswerWords.Add(DictionaryIndex.R_Exit, "\"salir\"");
            AnswerWords.Add(DictionaryIndex.R_Next, "\"siguiente\"");
            AnswerWords.Add(DictionaryIndex.R_Prev, "\"anterior\"");
            AnswerWords.Add(DictionaryIndex.R_Person, "\"wisdom.person\"");
            AnswerWords.Add(DictionaryIndex.R_Weather, "\"weather.search\"");
            AnswerWords.Add(DictionaryIndex.R_Joke, "\"chiste\"");
            AnswerWords.Add(DictionaryIndex.R_SmallTalk, "\"smalltalk.*\"");
            AnswerWords.Add(DictionaryIndex.R_AlarmClock, "\"alarma\"");
            AnswerWords.Add(DictionaryIndex.R_HoursAndMinutes, "\"horasminutos\"");
            AnswerWords.Add(DictionaryIndex.R_HoursOclock, "\"hoursenpunto\"");
            AnswerWords.Add(DictionaryIndex.R_Confirmation, "\"si\"");
            AnswerWords.Add(DictionaryIndex.R_Negation, "\"no\"");
            AnswerWords.Add(DictionaryIndex.R_Avatar, "\"avatar\"");
            #endregion

        }
        
    }
}
