﻿using UnityEngine;
using System.Collections;

namespace Digalix.Dictionary {

    public static class DictionaryIndex
    {
        #region Answers
        public static string A_DontUnderstand = "DontUnderstand";
        public static string A_TryAgain = "TryAgain";
        public static string A_StateMainMenu = "StateMainMenu";
        public static string A_QuestionHours = "TimeWakeUp";
        public static string A_WeareinMainMenu = "WeareinMainMenu";
        public static string A_RepeatAlarm = "RepatAlarm";
        public static string A_AlarmOnlyToday = "AlarmToday";
        public static string A_SaveConfiguration = "SaveConfiguration";
        public static string A_BackToAlarmClock = "BackToaAlarmClock";
        public static string A_Understand = "Understand";
        public static string A_Oclock = "Oclock";
        public static string A_UnavailableHour = "UnavailableHour";
        public static string A_HoursAnd = "HoursAnd";
        public static string A_AndMinutes = "AndMinutes";
        #endregion

        #region Requests
        public static string R_Dance = "Dance";
        public static string R_HyLola = "HyLola";
        public static string R_Exit = "Exit";
        public static string R_Next = "Next";
        public static string R_Prev = "Previous";
        public static string R_Person = "Person";
        public static string R_Weather = "Weather";
        public static string R_Joke = "Joke";
        public static string R_SmallTalk = "Smalltalk";
        public static string R_AlarmClock = "AlarmClock";
        public static string R_HoursAndMinutes = "HoursMinutes";
        public static string R_HoursOclock = "hoursoclock";
        public static string R_Confirmation = "Confirmation";
        public static string R_Negation = "Negation";
        public static string R_Avatar = "Avatar";
        #endregion

        #region Animations
        public static string T_Idle1 = "idle";
        public static string T_Idle2 = "idle2";
        public static string T_Idle3 = "idle3";
        public static string T_Listen = "listen";
        public static string T_Looking = "looking";
        public static string T_Warming = "warming";
        public static string T_Talking1 = "talking";
        public static string T_Talking2 = "talking_mirrored";
        public static string T_Kiss = "blow_a_kiss";
        public static string T_No = "no";
        public static string T_Dance = "dance";
        public static string T_gangnam = "gangnam";
        #endregion
    }

}