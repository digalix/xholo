﻿
namespace Digalix.Constants
{
    public static class Constants
    {
        /// <summary>
        /// Tolerancia al ruido, sirve para desencadenar el envío de audio a API.AI
        /// </summary>
        public const int TOLERANCE = 20;
        /// <summary>
        /// Segundos que dura la grabación para consultas cortas del tipo "Hola, Lola" "Siguiente" "Anterior", etc
        /// </summary>
        public const int SECONDS_SHORT_REQUEST = 3;
        /// <summary>
        /// Segundos que dura la grabación para consultas más largas del tipo "Quién es.....?" "Que tiempo hace en......";
        /// </summary>
        public const int SECONDS_LONG_REQUEST = 4;

        public const int HOURS_A_DAY = 23;

        public const int MINUTES_PER_HOUR = 59;
        
    }
}
