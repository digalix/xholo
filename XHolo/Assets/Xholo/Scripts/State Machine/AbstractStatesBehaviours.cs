﻿using UnityEngine;
using System.Collections;
using Digalix.StateMachine;
using Digalix.enums;
using Digalix.Dictionary;
using Digalix.Constants;

/// <summary>
/// Lantilla de la que heradan el resto de métodos de los diferentes estados
/// </summary>
public abstract class AbstractStatesBehaviours : MonoBehaviour
{
    public TTSController ttsController;
    protected Xholo xholo;
    protected StateMachine stateMachine;
    protected Animation anim;
    protected AudioSource audioSource;
    protected UIController uiController;
    public SplashScreen splash;
    public MainMenuVoiceController MMVoiceController;
    public MainMenuUIController MMUIController;

    //public UIController uiController;
    // Use this for initialization
    protected virtual void Start()
    {
        ttsController = GetComponent<TTSController>();
        xholo = GetComponent<Xholo>();
        stateMachine = GetComponent<StateMachine>();
        anim = GetComponent<Animation>();
        audioSource = GetComponent<AudioSource>();
        uiController = FindObjectOfType<UIController>();
        splash = FindObjectOfType<SplashScreen>();
        MMVoiceController = GetComponent<MainMenuVoiceController>();
        
    }

    public virtual void Actions (string theme, string question, string answer)
    {

    }

    public virtual void Actions(string theme, string question, string answer, string parameterA)
    {

    }

    public virtual void Actions(string theme, string question, string answer, string parameterA, string parameterB)
    {

    }

    /// <summary>
    /// Corrutina para que no se pare la animación si detecta un ruido
    /// </summary>
    /// <param name="animationName"></param>
    /// <returns></returns>
    /// 
    protected virtual IEnumerator WaitForAnimation(string animationName)
    {
        yield return new WaitUntil(() => !anim.IsPlaying(animationName)&&(splash.LoadingImage.fillAmount == 1));
        ttsController.OnEnglishButtonWomanPressed();
        audioSource.Stop();
        xholo.StartListening();
        if ((xholo.splashscreen))
        {
            uiController.SplashScreen.enabled = false;
        }
        anim.CrossFade(DictionaryIndex.T_Idle1);
    }
    /// <summary>
    /// Corrutina para que se pare la animación a la vez que un audio (canciones, etc)
    /// </summary>
    /// <param name="audioLength"></param>
    /// <returns></returns>
    protected virtual IEnumerator WaitForAudio(float audioLength)
    {
        yield return new WaitForSeconds(audioLength);
        Debug.Log(audioLength);
        audioSource.Stop();
        anim.CrossFade(DictionaryIndex.T_Idle1);
        xholo.StartListening();
    }
}
