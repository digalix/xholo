﻿using UnityEngine;
using System.Collections;
using Digalix.StateMachine;
using Digalix.enums;
using Digalix.Dictionary;

public class MainMenuBehaviour : AbstractStatesBehaviours {
    
    public AudioClip gangnam;
    

    public override void Actions(string theme, string question, string answer)
    {
        
        if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_Dance])
        {
            audioSource.clip = gangnam;
            audioSource.Play();
            audioSource.volume = 1;
            anim.CrossFade(DictionaryIndex.T_gangnam);
            float audioLength = audioSource.clip.length;
            StartCoroutine(WaitForAudio(audioLength));
        }
        else if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_Person])
        {
            ttsController.Speak(answer);
        }
        else if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_Weather])
        {
            ttsController.Speak(answer);
        }
        else if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_SmallTalk])
        {
            ttsController.Speak(answer);
        }
        else if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_Joke])
        {
            ttsController.Speak(answer);
        }
        else if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_AlarmClock])
        {
            //ttsController.Speak(answer);
            ttsController.Speak(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_QuestionHours]);
            stateMachine.CurrentState = State.AlarmClock;
        }
        else if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_Exit])
        {
            ttsController.Speak(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_StateMainMenu]);
            stateMachine.CurrentState = State.Idle;
        }
        else
        {
            xholo.StartListening();
        } 
        
    }
    
}
