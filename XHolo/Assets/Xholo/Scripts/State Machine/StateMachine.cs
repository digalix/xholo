﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Digalix.enums;
using Digalix.Dictionary;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;
using ApiAiSDK;
using ApiAiSDK.Model;
using ApiAiSDK.Unity;

namespace Digalix.StateMachine
{
    public class StateMachine : MonoBehaviour
    {
        public Animation Anim;
        public State CurrentState;
        public MainMenuVoiceController XholoComponent;
        public Xholo XholoLola;
        public UIController uiController;
        public MainMenuUIController MMUICOntroller;
        private MainMenuBehaviour mainMenuBehaviour;
        private IdleBehaviour idelBehavour;
        private AlarmClockBehaviour alarmClockBehaviour;
        private AlarmClockConfirmationBehaviour alarmClockConfirmation;
        private RepeatAlarmBehaviour repeatalarm;
        private MenuIdleBehaviour menuIdleBehaviour;
        private ChooseLanguajeBehaviour chooseLanguajeBehaviour;
        private AvatarSellectionBehaviour avatarSellectionBehaviour;
        private TTSController ttscontroller;
        public string theme;
        public string answer;
        public string question;
        public string ParameterA;
        public string ParameterB;


        void Start()
        {
            
            if (uiController != null)
            {
                CurrentState = State.Idle;
            }
            else
            {
                CurrentState = State.ChooseLanguaje;
            }
            
            XholoComponent = GetComponent<MainMenuVoiceController>();
            XholoLola = GetComponent<Xholo>();
            Anim = GetComponent<Animation>();
            idelBehavour = GetComponent<IdleBehaviour>();
            mainMenuBehaviour = GetComponent<MainMenuBehaviour>();
            alarmClockBehaviour = GetComponent<AlarmClockBehaviour>();
            alarmClockConfirmation = GetComponent<AlarmClockConfirmationBehaviour>();
            menuIdleBehaviour = GetComponent<MenuIdleBehaviour>();
            chooseLanguajeBehaviour = GetComponent<ChooseLanguajeBehaviour>();
            ttscontroller = GetComponent<TTSController>();
            repeatalarm = GetComponent<RepeatAlarmBehaviour>();
            avatarSellectionBehaviour = GetComponent<AvatarSellectionBehaviour>();
            Debug.Log(XholoLola.Words.xholoDictionary.answerwords[DictionaryIndex.R_HoursOclock]);
        }
        

        public void NewState(AIResponseEventArgs e)
        {
            var aiResponse = e.Response;
            theme = JsonConvert.SerializeObject(aiResponse.Result.Action);
            answer = JsonConvert.SerializeObject(aiResponse.Result.Fulfillment.Speech);
            question = JsonConvert.SerializeObject(aiResponse.Result.ResolvedQuery);
            if (CurrentState == State.AlarmClock)
            {
                if (aiResponse.Result.HasParameters)
                {
                    if (aiResponse.Result.Parameters.Count == 1)
                    {
                        ParameterA = JsonConvert.SerializeObject(aiResponse.Result.Parameters["number"]);
                    }
                    else if (aiResponse.Result.Parameters.Count == 2)
                    {
                        ParameterA = JsonConvert.SerializeObject(aiResponse.Result.Parameters["number"]);
                        ParameterB = JsonConvert.SerializeObject(aiResponse.Result.Parameters["number2"]);
                    }
                }
            }
            SwitchCurrentState();
        }

        public void SwitchCurrentState()
        {
            if (uiController != null)
            {
                switch (CurrentState)

                {
                case State.Idle:
                    idelBehavour.Actions(theme, question, answer);
                    break;
                case State.MainMenu:
                    mainMenuBehaviour.Actions(theme, question, answer);
                    break;
                case State.AlarmClock:
                        //uiController.AlarmClockCanvas.enabled = true;
                        if (theme == XholoLola.Words.xholoDictionary.answerwords[DictionaryIndex.R_HoursOclock])
                    {
                        uiController.AlarmClockCanvas.enabled = true;
                        alarmClockBehaviour.Actions(theme, question, answer, ParameterA);

                    } else if (theme == XholoLola.Words.xholoDictionary.answerwords[DictionaryIndex.R_HoursAndMinutes])
                    {
                        uiController.AlarmClockCanvas.enabled = true;
                        alarmClockBehaviour.Actions(theme, question, answer, ParameterA, ParameterB);
                    } else if (theme == XholoLola.Words.xholoDictionary.answerwords[DictionaryIndex.R_Exit])
                    {
                        uiController.AlarmClockCanvas.enabled = false;
                        ttscontroller.Speak(XholoLola.Words.xholoDictionary.answerwords[DictionaryIndex.A_WeareinMainMenu]);
                        CurrentState = State.MainMenu;
                    }
                    else
                    {
                        Anim[DictionaryIndex.T_No].wrapMode = WrapMode.Once;
                        Anim.CrossFade(DictionaryIndex.T_No);
                        StartCoroutine(WaitForAnimation(DictionaryIndex.T_No));
                    }
                    break;
                case State.AlarmClockConfirmation:
                    alarmClockConfirmation.Actions(theme, question, answer);
                    break;
                case State.RepeatAlarmConfirmation:
                    repeatalarm.Actions(theme, question, answer);
                    break;
                    default:
                    break;
                }
            }
            else
            {
                switch (CurrentState)
                {
                    case State.ChooseLanguaje:
                        chooseLanguajeBehaviour.Actions(theme, question, answer);
                        break;
                    case State.MenuIdle:
                        menuIdleBehaviour.Actions(theme, question, answer);
                        break;
                    case State.AvatarSellection:
                        avatarSellectionBehaviour.Actions(theme, question, answer);
                        break;
                    default:
                        break;
                }
            }
            
        }
            
        /// <summary>
        /// Corrutina para que no se pare la animación si detecta un ruido
        /// </summary>
        /// <param name="animationName"></param>
        /// <returns></returns>
        private IEnumerator WaitForAnimation(string animationName)
        {
            yield return new WaitUntil(() => !Anim.IsPlaying(animationName));
            Anim.CrossFade(DictionaryIndex.T_Idle1);
            XholoComponent.StartListening();
        }
    }
}
