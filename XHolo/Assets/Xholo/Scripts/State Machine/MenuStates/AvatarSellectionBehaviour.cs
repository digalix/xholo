﻿using UnityEngine;
using System.Collections;
using Digalix.Dictionary;

public class AvatarSellectionBehaviour : AbstractStatesBehaviours {
    
    public override void Actions(string theme, string question, string answer)
    {
        if (theme == MMVoiceController.Words.xholoDictionary.answerwords[DictionaryIndex.R_Next])
        {
            MMUIController.OnNextAvatarButtonPressed();
            MMVoiceController.StartListening();
        }else if (theme == MMVoiceController.Words.xholoDictionary.answerwords[DictionaryIndex.R_Prev])
        {
            MMUIController.OnPreviousAvatarButtonPressed();
            MMVoiceController.StartListening();
        }
        else
        {
            MMVoiceController.StartListening();
        }
    }

}
