﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChooseLanguajeBehaviour : AbstractStatesBehaviours {

    public bool Firstsend = false;
    public SplashScreen SScreen;
    public Canvas SplashCanvas;

	public override void Actions (string theme, string question, string answer)
    {
        Firstsend = true;
        if (Firstsend)
        {
            StartCoroutine(WaitToResponse());
        }
        Debug.Log(theme);
        MMVoiceController.StartListening();
    }

    public IEnumerator WaitToResponse()
    {
        yield return new WaitUntil(() => SScreen.LoadingImage.fillAmount == 1);
        SplashCanvas.enabled = false;
    }
}
