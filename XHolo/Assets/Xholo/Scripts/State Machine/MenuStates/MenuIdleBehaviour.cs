﻿using UnityEngine;
using System.Collections;
using Digalix.Dictionary;
using Digalix.StateMachine;
using Digalix.enums;

public class MenuIdleBehaviour : AbstractStatesBehaviours {
    
    public override void Actions(string theme, string question, string answer)
    {
        if (theme == MMVoiceController.Words.xholoDictionary.answerwords[DictionaryIndex.R_Avatar])
        {
            MMUIController.OnAvatarButtonPressed();
            MMVoiceController.StartListening();
            stateMachine.CurrentState = State.AvatarSellection;
        }
        else
        {
            MMVoiceController.StartListening();
        }
    }


}
