﻿using UnityEngine;
using System.Collections;
using Digalix.Dictionary;
using Digalix.enums;

public class AlarmClockConfirmationBehaviour : AbstractStatesBehaviours {

    private AlarmClockBehaviour alarmClockBehaviour;    

    public override void Actions(string theme, string question, string answer)
    {
        alarmClockBehaviour = GetComponent<AlarmClockBehaviour>();
        
        if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_Confirmation])
        {
            AlarmClockConfiguration.Hour = alarmClockBehaviour.InputHours.text;
            AlarmClockConfiguration.Minutes = alarmClockBehaviour.InputMinutes.text;
            ttsController.Speak(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_SaveConfiguration]);
            stateMachine.CurrentState = State.RepeatAlarmConfirmation;
        }
        else if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_Negation])
        {
            ttsController.Speak(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_BackToAlarmClock] + xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_QuestionHours]);
            stateMachine.CurrentState = State.AlarmClock;
        }
        else
        {
            ttsController.Speak(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_TryAgain]);
        }

    }
}
