﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Digalix.StateMachine;
using Digalix.enums;
using Digalix.Dictionary;
using Digalix.Constants;

public class AlarmClockBehaviour : AbstractStatesBehaviours {

    public InputField InputHours;
    public InputField InputMinutes;

    public bool AlarmActive = false;

    public override void Actions(string theme, string question, string answer, string parameterA)
    {
        string hour = parameterA;
        ttsController.Speak(hour);
        InputHours.text = String.Format("{0}", hour.Replace('"', ' ').Trim());
        int hourToInt = 0;
        bool hoursOk = AcceptableTime(hourToInt,InputHours.text,true);
        InputMinutes.text = "00";

        if (hoursOk)
        {
            ttsController.Speak(string.Format(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_Understand] + "{0}" + xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_Oclock], parameterA));
            stateMachine.CurrentState = State.AlarmClockConfirmation;
        }
        else
        {
            ttsController.Speak(string.Format(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_UnavailableHour] + xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_QuestionHours]));
            stateMachine.CurrentState = State.AlarmClock;
        }
    }

    public override void Actions(string theme, string question, string answer, string parameterA, string parameterB)
    {
        string hour = parameterA;
        string minutes = parameterB;

        InputHours.text = String.Format("{0}", hour.Replace('"', ' ').Trim());
        InputMinutes.text = String.Format("{0}", minutes.Replace('"', ' ').Trim());
        int hourToInt = 0;
        int minutesToInt = 0;
        bool hoursOk = AcceptableTime(hourToInt, InputHours.text, true) ;
        bool minutesOK = AcceptableTime(minutesToInt, InputMinutes.text, false);

        if ((hoursOk)&&(minutesOK))
        {
            ttsController.Speak(string.Format(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_Understand] + "{0}" + xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_HoursAnd] + "{1}" + xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_AndMinutes], parameterA, parameterB));
            stateMachine.CurrentState = State.AlarmClockConfirmation;
        }
        else
        {
            ttsController.Speak(string.Format(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_UnavailableHour] + xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_QuestionHours]));
            stateMachine.CurrentState = State.AlarmClock;
        }

    }

    
    private bool AcceptableTime(int NumberToConvert, string InputFieldToShow, bool IsHour)
    {
        bool TimeOk;

        try
        {
            NumberToConvert = Convert.ToInt32(InputFieldToShow);
        }
        catch (FormatException e)
        {
            Debug.Log("[AlarmClockBehaviour]: La cadena de MINUTOS no puede ser convertida a entero");
            TimeOk = false;
        }
        catch (OverflowException e)
        {
            Debug.Log("[AlarmClockBehaviour]: La cadena de MINUTOS transformada no cabe en el entero");
            TimeOk = false;
        }
        finally
        {
            if (IsHour == true)
            {
                if (NumberToConvert < Constants.HOURS_A_DAY)
                {
                    TimeOk = true;
                    InputHours.text = NumberToConvert.ToString("00");
                }
                else
                {
                    TimeOk = false;
                }
            }
            else
            {
                if (NumberToConvert < Constants.MINUTES_PER_HOUR)
                {
                    TimeOk = true;
                    InputMinutes.text = NumberToConvert.ToString("00");
                }
                else
                {
                    TimeOk = false;
                }
            }
        }
        return TimeOk;
    }

}
