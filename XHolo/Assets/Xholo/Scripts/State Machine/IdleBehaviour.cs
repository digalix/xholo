﻿using UnityEngine;
using System.Collections;
using Digalix.StateMachine;
using Digalix.enums;
using Digalix.Dictionary;

public class IdleBehaviour : AbstractStatesBehaviours {
    
    public override void Actions (string theme, string question, string answer)
    {
        if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_HyLola])
        {
            ttsController.Speak(answer);
            stateMachine.CurrentState = State.MainMenu;
        }
        else
        {
            anim[DictionaryIndex.T_No].wrapMode = WrapMode.Once;
            anim.CrossFade(DictionaryIndex.T_No);
            StartCoroutine(WaitForAnimation(DictionaryIndex.T_No));
           
        }
    }
   

}
