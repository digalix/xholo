﻿using UnityEngine;
using System.Collections;
using Digalix.Dictionary;
using Digalix.enums;

public class RepeatAlarmBehaviour : AbstractStatesBehaviours {
    public AlarmClock alarmClock;
    public override void Actions(string theme, string question, string answer)
    {
        if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_Confirmation])
        {
            ttsController.Speak(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_RepeatAlarm]);
            AlarmClockConfiguration.Repeat = true;
            alarmClock.enabled = true;
            stateMachine.CurrentState = State.MainMenu;
        }
        else if (theme == xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_Negation])
        {
            ttsController.Speak(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_AlarmOnlyToday]);
            AlarmClockConfiguration.Repeat = false;
            alarmClock.enabled = true;
            stateMachine.CurrentState = State.MainMenu;
            
        }
        else
        {
            ttsController.Speak(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.A_TryAgain]);
        }
    }

}
