﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using Digalix.StateMachine;
using Digalix.enums;

public class AlarmClock : MonoBehaviour {

    private string timeNow;
    private TTSController ttsController;
    public string AlarmHour;
    public string AlarmMinutes;
    public UIController uiController;
    public StateMachine SMachine;
    public AudioSource Asource;
    public Xholo Xh;
    public bool StartAlarm;
    public bool RepeatAlarm;
    public Text HourNowText;
    public Text ProgrammedHourText;
    public Text Alarm;
    
    
    
	void Start () {
        StartAlarm = false;
        AlarmHour = AlarmClockConfiguration.Hour;
        AlarmMinutes = AlarmClockConfiguration.Minutes;
        RepeatAlarm = AlarmClockConfiguration.Repeat;
        ttsController = GetComponent<TTSController>();
        Asource = GetComponent<AudioSource>();
        Xh = GetComponent<Xholo>();
        ProgrammedHourText.text = String.Format("{0}:{1}:00",AlarmHour, AlarmMinutes);
        StartCoroutine(Timer());
    }
	
    public IEnumerator Timer()
    {
        yield return new WaitForSeconds(1);
        timeNow = DateTime.Now.ToString("HH:mm:ss");
        HourNowText.text = timeNow;
        if ((timeNow == string.Format("{0}:{1}:00", AlarmHour, AlarmMinutes)) && (!StartAlarm))
        {
            ActivateAlarm(true);
        }
        StartCoroutine(Timer());
    }

    public void ActivateAlarm(bool active)
    {
        if (active)
        {
            uiController.StopAlarmCanvas.enabled = true;
            StartAlarm = true;
            Alarm.enabled = true;
            Asource.Stop();
            StartCoroutine(WakeUP());
            SMachine.CurrentState = State.MainMenu;
        }
        else
        {
            uiController.StopAlarmCanvas.enabled = false;
            StartAlarm = false;
            Alarm.enabled = false;            
        }
    }

    public IEnumerator WakeUP()
    {
        
        if (StartAlarm)
        {
            yield return new WaitForSeconds(3);
            ttsController.Speak("PLEASE, WAKE UP!!");
            StartCoroutine(WakeUP());
        }
        
    }

    

	
}
