﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public static class AlarmClockConfiguration 
{
    public static string Hour;
    public static string Minutes;
    public static bool AlarmActive;
    public static bool Repeat;

}

