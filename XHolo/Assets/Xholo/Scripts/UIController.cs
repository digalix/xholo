﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Digalix.StateMachine;
using Digalix.enums;

public class UIController : MonoBehaviour {

    public AlarmClock alarmClock;
    public StateMachine stateMachine;
    public Xholo xholo;
    public Text Question;
    public Text Answer;
    public Text Theme;
    public Text State;
    public Button AcceptAlarmButton;
    public InputField HourInput;
    public InputField MinutesInput;
    public Canvas AlarmClockCanvas;
    public Canvas StopAlarmCanvas;
    public Canvas SplashScreen; 
   

	// Update is called once per frame
	public void Update () {
        Question.text = stateMachine.question;
        Answer.text = stateMachine.answer;
        Theme.text = stateMachine.theme;
        State.text = stateMachine.CurrentState.ToString();
    }

    public void OnStopAlarmButtonPressed()
    {
        alarmClock.ActivateAlarm(false);
        if (!alarmClock.RepeatAlarm)
        {
            alarmClock.enabled = false;
            xholo.StartListening();
        }
    }


}
