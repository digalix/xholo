﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using Digalix.StateMachine;
using Digalix.enums;

public class MainMenuUIController : MonoBehaviour {
    
    public Canvas LenguageSelection;
    public Canvas MainMenu;
    public Canvas ChooseAvatar;
    public Canvas ChangeUser;
    public MainMenuController MMController;
    public StateMachine SMachine;

    public void OnEnglishButtonPressed()
    {
        LenguageSelection.enabled = false;
        MainMenu.enabled = true;
        MMController.SelectedAvatar.SetActive(true);
        SMachine.CurrentState = State.MenuIdle;
    }

    public void OnSpanishButtonPressed()
    {
        LenguageSelection.enabled = false;
        MainMenu.enabled = true;
    }

    public void OnExitMainMenuButtonPressed()
    {
        Application.Quit();
    }
    
    public void OnAvatarButtonPressed()
    {
        MainMenu.enabled = false;
        ChooseAvatar.enabled = true;
        SMachine.CurrentState = State.AvatarSellection;
    }

    public void OnAdminUsersButtonPressed()
    {
        MainMenu.enabled = false;
        ChangeUser.enabled = true;
        MMController.SelectedAvatar.SetActive(false);

    }

    public void OnStartXHoloButtonPressed()
    {
        SceneManager.LoadScene("Xholo_3D");
    }

    public void OnExitChangeUsersButtonPressed()
    {
        ChangeUser.enabled = false;
        MainMenu.enabled = true;
    }

    public void OnAcceptChangeUsersButtonPRessed()
    {
        ChangeUser.enabled = false;
        MainMenu.enabled = true;
    }

    public void OnExitChooseAvatarButtonPressed()
    {
        ChooseAvatar.enabled = false;
        MainMenu.enabled = true;
    }

    public void OnAcceptChooseAvatarButtonPressed()
    {
        ChooseAvatar.enabled = false;
        MainMenu.enabled = true;
    }

    public void OnNextAvatarButtonPressed()
    {
        MMController.Avatars[MMController.AvatarActive].SetActive(false);
        MMController.AvatarActive++;
        if (MMController.AvatarActive > MMController.Avatars.Length - 1)
        {
            MMController.AvatarActive = 0;
        }
        MMController.Avatars[MMController.AvatarActive].SetActive(true);
    }

    public void OnPreviousAvatarButtonPressed()
    {
        MMController.Avatars[MMController.AvatarActive].SetActive(false);
        MMController.AvatarActive--;
        if (MMController.AvatarActive < 0)
        {
            MMController.AvatarActive = MMController.Avatars.Length - 1;
        }
        MMController.Avatars[MMController.AvatarActive].SetActive(true);
    }
}
