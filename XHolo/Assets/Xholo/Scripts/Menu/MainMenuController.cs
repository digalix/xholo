﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {

    public GameObject[] Avatars;
    public GameObject SelectedAvatar;
    public int AvatarActive;

    public void Awake()
    {
        AvatarActive = 0;
        SelectedAvatar = Avatars[AvatarActive];
    }
	
}
