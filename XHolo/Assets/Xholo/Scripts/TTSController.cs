﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using ApiAiSDK.Unity;
using Digalix.Dictionary;
using System.Text;
using System.Text.RegularExpressions;

public class TTSController : MonoBehaviour {
    
    private bool hasInit;
    private Xholo xholo;
    private TextToSpeechPlugin textToSpeechPlugin;
    private string[] localeCountryISO2AlphaSet = null;
    private SpeechPlugin speechPlugin;
    private bool tryAgain = false;
    private Animation anim;
    void Start () {
        xholo = GetComponent<Xholo>();
        textToSpeechPlugin = TextToSpeechPlugin.GetInstance();
        //apiAiUnity = new ApiAiUnity();
        anim = GetComponent<Animation>();
        textToSpeechPlugin.SetDebug(0);
        //textToSpeechPlugin.LoadLocaleCountry();
        //textToSpeechPlugin.LoadCountryISO2AlphaCountryName();
        //UpdateLocale(SpeechLocale.US);//para iniciar con voz de mujer en inglés, puesto en Xholo.cs a partir de la función del botón
        //OnEnglishButtonWomanPressed();//puesto en xholo.cs
        textToSpeechPlugin.Init();
        textToSpeechPlugin.SetTextToSpeechCallbackListener(OnInit, OnGetLocaleCountry, OnSetLocale, OnStartSpeech, OnDoneSpeech, OnErrorSpeech);

    }
	

	public void Update () {

	}
    private void OnApplicationPause(bool val)
    {
        if (textToSpeechPlugin != null)
        {
            if (hasInit)
            {
                if (val)
                {
                    textToSpeechPlugin.UnRegisterBroadcastEvent();
                }
                else
                {
                    textToSpeechPlugin.RegisterBroadcastEvent();
                }
            }
        }
    }
    // Update is called once per frame

    public void UpdateLocale(SpeechLocale locale)
    {
        textToSpeechPlugin = TextToSpeechPlugin.GetInstance();
        textToSpeechPlugin.SetLocale(locale);
    }
    
    private void OnInit(int status)
    {
        Debug.Log("[TextToSpeechDemo] OnInit status: " + status);

        if (status == 1)
        {
            hasInit = true;
            textToSpeechPlugin.GetAvailableLocale();
        }
        else
        {

        }
    }

    private void OnGetLocaleCountry(string localeCountry)
    {
        localeCountryISO2AlphaSet = localeCountry.Split(',');
    }

    private bool CheckLocale(TTSLocaleCountry ttsCountry)
    {
        bool found = false;
        if (localeCountryISO2AlphaSet != null)
        {
            string countryISO2Alpha = textToSpeechPlugin.GetCountryISO2Alpha(ttsCountry);

            foreach (string country in localeCountryISO2AlphaSet)
            {
                if (country.Equals(countryISO2Alpha, StringComparison.Ordinal))
                {
                    found = true;
                    break;
                }
            }
        }

        return found;
    }

    private void OnSetLocale(int status)
    {
        Debug.Log("[TextToSpeech] OnSetLocale status: " + status);
        if (status == 1)
        {

        }
        else
        {

        }
    }

    private void OnStartSpeech(string utteranceId)
    {
        Debug.Log("[TextToSpeech] OnStartSpeech utteranceId: " + utteranceId);
        int random = UnityEngine.Random.Range(0, 2);
        switch (random)
        {
            case 0:
                anim.CrossFade(DictionaryIndex.T_Talking1);
                break;
            case 1:
                anim.CrossFade(DictionaryIndex.T_Talking2);
                break;
        }
        StartCoroutine(WaitToListen());
        OnStartSpeechBehavior();
    }

    private void OnDoneSpeech(string utteranceId)
    {
        UpdateStatus("Done Speech...");
        Debug.Log("[TextToSpeech] OnDoneSpeech utteranceId: " + utteranceId);
        anim.CrossFade(DictionaryIndex.T_Idle1);
        OnDoneSpeechBehavior();
        tryAgain = true;
    }

    public void OnStartSpeechBehavior()
    {

    }

    public void OnDoneSpeechBehavior()
    {

    }
    
    public IEnumerator WaitToListen()
    {
        yield return new WaitUntil(() => tryAgain == true);
        tryAgain = false;
        xholo.StartListening();
        
    }

    private void OnErrorSpeech(string utteranceId)
    {
        Debug.Log("[TextToSpeech] OnErrorSpeech utteranceId: " + utteranceId);
        //xholo.StartListening();
    }

    private void UpdateStatus(string status)
    {

    }

    public void Speak(string whatToSay)
    {

        string utteranceId = "test-utteranceId";
        if (hasInit)
        {
            /*string normalizedText = whatToSay.Normalize(NormalizationForm.FormD);
            Regex reg = new Regex("[^a-zA-Z0-9 ]");
            string sinAcentos = reg.Replace(normalizedText, "");*/
            UpdateStatus("Trying to speak...");
            Debug.Log("[TextToSpeech] SpeakOut whatToSay: " + " utteranceId " + utteranceId);
            textToSpeechPlugin.SpeakOut(whatToSay, utteranceId);
        }
    }


    public void OnEnglishButtonManPressed()
    {
        UpdateLocale(SpeechLocale.UK);
        float pitch = 1f;
        textToSpeechPlugin.SetPitch(pitch);
    }

    public void OnEnglishButtonWomanPressed()
    {
        UpdateLocale(SpeechLocale.US);
        float pitch = 1f;
        textToSpeechPlugin.SetPitch(pitch);

        Debug.Log("Idioma cambiado a Inglés");
    }

    public void OnSpanishButtonWomanPressed()
    {
        textToSpeechPlugin = TextToSpeechPlugin.GetInstance();
        textToSpeechPlugin.SetLocaleByCountry("EC");
        float pitch = 1f;
        textToSpeechPlugin.SetPitch(pitch);
    }

    public void OnSpanishButtonManPressed()
    {
        textToSpeechPlugin = TextToSpeechPlugin.GetInstance();
        textToSpeechPlugin.SetLocaleByCountry("EC");
        float pitch = 0.50f;
        textToSpeechPlugin.SetPitch(pitch);

    }
}
