﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
namespace Digalix
{

    public class RecorrerAvatares : MonoBehaviour
    {

        public Button Next;
        public GameObject[] Avatars;
        private int avatarActive;
        // Use this for initialization
        void Start()
        {
            avatarActive = 0;
            Avatars[avatarActive].SetActive(true);
            //Avatars[1].SetActive(false);
        }

        
        public void OnNextButtonPressed()
        {
            Avatars[avatarActive].SetActive(false);
            avatarActive++;
            if (avatarActive > Avatars.Length - 1)
            {
                avatarActive = 0;
            }
            Avatars[avatarActive].SetActive(true);
        }

        public void OnBackButtonPressed()
        {
            Avatars[avatarActive].SetActive(false);
            avatarActive--;
            if (avatarActive < 0)
            {
                avatarActive = Avatars.Length - 1;
            }
            Avatars[avatarActive].SetActive(true);
        }

    }
}
