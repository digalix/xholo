

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using ApiAiSDK;
using ApiAiSDK.Unity;
using Newtonsoft.Json;
using System.Net;
using System.Collections.Generic;
using Digalix;
using Digalix.Constants;
using Digalix.StateMachine;
using Digalix.Dictionary;
using System.Text.RegularExpressions;
using System.Text;


public class MainMenuVoiceControl : MonoBehaviour {
    
    public ContextDictionary Words;
    private TTSController ttsController;
    private AudioSource aud;
    private bool listening;
    public ApiAiUnity apiAiUnity;
    public RecorrerAvatares recorrerAvatares;
    public string completeResponse; 
    public string theme;
    public string question;


    /*private readonly JsonSerializerSettings jsonSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
        };*/

    private readonly Queue<Action> ExecuteOnMainThread = new Queue<Action>();
    void Awake()
    {
        Words = new ContextDictionary(new ENDictionary());
        Words.xholoDictionary.StartDictionary();
    }
    
        IEnumerator Start()
        {
        //Instancia del diccionario, para poder cambiar de idiomas dependiendo del UserProfile
        //TODO integraci�n del userprofile
       
            //Debug.Log("prueba de diccionario!! " + Words.xholoDictionary.answerwords[DictionaryIndex.A_DontUnderstand]);
            
            // Zona de inicializaci�n de API.AI
            // Comprobar el acceso al micr�fono
            yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);
            if (!Application.HasUserAuthorization(UserAuthorization.Microphone))
            {
                throw new NotSupportedException("Microphone using not authorized");
            }

            ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) =>
            {
                return true;
            };
            //Tokens de API.AI para hacer las consultas con la AI customizada
            const string SUBSCRIPTION_KEY_SP = "c778b029da07460dbda7a484d582dce0";
            const string ACCESS_TOKEN_SP = "063fdda3e8b444319feb2384db50ea0a";
            const string SUBSCRIPTION_KEY_UK = "7ec3d7d5c31e46e39fe81fa47e29d3a1";
            const string ACCESS_TOKEN_UK = "7b506eb141614cb68c0e0031ae992d6f";
            var config = new AIConfiguration(SUBSCRIPTION_KEY_UK, ACCESS_TOKEN_UK, SupportedLanguage.English);
            apiAiUnity = new ApiAiUnity();
            apiAiUnity.Initialize(config);
            apiAiUnity.OnError += HandleOnError;
            apiAiUnity.OnResult += HandleOnResult;
            recorrerAvatares = GetComponent<RecorrerAvatares>();
            if (Application.platform == RuntimePlatform.Android)
            {
                ttsController = GetComponent<TTSController>();
                ttsController.enabled = true;
            }
            //ttsController.OnEnglishButtonWomanPressed();//Para que salte en ingl�s directamente
            StartListening();
            StopListening();
        
        
    }

        public void Update()
        {
        //Debug.Log(apiAiUnity.loudness());
            if (apiAiUnity != null)
            {
                apiAiUnity.Update();
            }

            while (ExecuteOnMainThread.Count > 0)
            {
                ExecuteOnMainThread.Dequeue().Invoke();
            }
            if (listening)
            {
                //Configuraci�n del check con el bot�n de color rojo/verde
                //GreenButton.SetActive(true);
                //RedButton.SetActive(false);
                //Configuraci�n del l�mite de ruido para iniciar el env�o del audio
                if (apiAiUnity.loudness() > Constants.TOLERANCE)
                {
                    //StartCoroutine(WaitToLoud());
                    //GreenButton.SetActive(false);
                    //RedButton.SetActive(true);
                    StopListening();
                }
            }
        }

        #region Voice Recognition
    /// <summary>
    /// Inicia la grabaci�n de audio, genera una cola que se va deshaciendo de lo m�s antiguo
    /// </summary>
        public void StartListening()
        {
            Debug.Log("StartListening");
            aud = GetComponent<AudioSource>();
            apiAiUnity.StartListening(aud);
            listening = true;

        }
    /// <summary>
    /// Para la grabaci�n de audio y env�a lo almacenado en los �ltimos X segundos (a configurar en el ApiAiUnity)
    /// </summary>
        public void StopListening()
        {
            try
            {
                Debug.Log("StopListening");
                apiAiUnity.StopListening();
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            listening = false;
        }
    #endregion

            
    #region StateControl
    /// <summary>
    /// Devuelve el resultado de la consulta en formato "request", incluye las comillas, hay que tenerlo en cuenta a la hora de comparar "\"request\""
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void HandleOnResult(object sender, AIResponseEventArgs e)
        {
            RunInMainThread(() =>
            {
                
                var aiResponse = e.Response;
                completeResponse = JsonConvert.SerializeObject(aiResponse);
                theme = JsonConvert.SerializeObject(aiResponse.Result.Action);
                //answer = JsonConvert.SerializeObject(aiResponse.Result.Fulfillment.Speech);
                question = JsonConvert.SerializeObject(aiResponse.Result.ResolvedQuery);
                //stateMachine.NewState(theme, question);
                //stateMachine.NewState(theme, question, answer);
                /*if (JsonConvert.SerializeObject(aiResponse.Result.Parameters.Values != null))
                {
                    string num = JsonConvert.SerializeObject(aiResponse.Result.Parameters.Values);
                }*/
                if (question == "\"next\"")
                {
                    recorrerAvatares.OnNextButtonPressed();
                }

                if (question == "\"back\"")
                {
                    recorrerAvatares.OnBackButtonPressed();
                }
                Debug.Log(completeResponse);
                StartListening();
                //Debug.Log(num);
                //Aqu� se env�a el nuevo estado al que queremos pasar en la m�quina de estados
                //StartListening(); 
            });
        }
    
    /// <summary>
    /// Respuesta en caso de error para que no cause un par�n de la aplicaci�n
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
        public void HandleOnError(object sender, AIErrorEventArgs e)
        {
            RunInMainThread(() =>
            {
                //error = e.Exception.ToString();
                Debug.LogException(e.Exception);
                Debug.Log(e.ToString());
                //ttsController.Speak(Words.xholoDictionary.answerwords[DictionaryIndex.A_TryAgain]);
                StartListening();
            });
        }
        private void RunInMainThread(Action action)
        {
            ExecuteOnMainThread.Enqueue(action);
        }
    #endregion
    public IEnumerator WaitToLoud()
    {
        yield return new WaitUntil(() => apiAiUnity.loudness() < 15);
        //GreenButton.SetActive(false);
        //RedButton.SetActive(true);
        StopListening();
    }

    public IEnumerator FirstSend()
    {
        yield return new WaitForSeconds(2);
        StopListening();
    }
}

