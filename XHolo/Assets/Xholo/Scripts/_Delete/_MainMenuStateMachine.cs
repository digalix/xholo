﻿using UnityEngine;
using System.Collections;
using Digalix.enums;
using Digalix.Dictionary;
using System.Text.RegularExpressions;
using System.Text;

namespace Digalix.StateMachine
{
    public class MainMenuStateMachine : MonoBehaviour
    {
        private MainMenuVoiceControl mainmenuVoiceControl;
        private State currentState;
             
        void Start()
        {
            mainmenuVoiceControl = GetComponent<MainMenuVoiceControl>();
            currentState = State.Idle;
            //Debug.Log(xholo.Words.xholoDictionary.answerwords[DictionaryIndex.R_Dance]);
        }
        /// <summary>
        /// Función por la que vamos pasando por estados 
        /// </summary>
        /// <param name="theme"></param>
        /// <param name="answer"></param>
        /// <param name="question"></param>
        public void NewState(string theme, string question)
        {
            
            if (currentState != State.MainMenu)
            {
                
            }

            if (currentState == State.MainMenu)
            {
                if (question == "next")
                {

                }
                else
                {
                    mainmenuVoiceControl.StartListening();
                }
            }
        }
        
        /// <summary>
        /// Estado al que pasamos una vez saludamos al avatar
        /// </summary>
        /// <param name="answer"></param>
        public void StateMainMenu(string answer)
        {
            
            currentState = State.MainMenu;
        }
        /// <summary>
        /// Estado base del avatar
        /// </summary>
        public void StateIdle()
        {
            //ttsController.OnStartSpeechBehavior();
            //ttsController.OnDoneSpeechBehavior();
            currentState = State.Idle;
        }
        /// <summary>
        /// Estado ejecutado cuando preguntamos por una persona
        /// </summary>
        /// <param name="answer"></param>
        public void WisdomPersonRespond(string answer)
        {

        }
        /// <summary>
        /// Corrutina para que no se pare la animación si detecta un ruido
        /// </summary>
        /// <param name="animationName"></param>
        /// <returns></returns>
        
    }
}
