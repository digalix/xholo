﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SplashScreen : MonoBehaviour {

    public Image LoadingImage;
    public float SplashCompleteSpeed;

	void Start () {
        LoadingImage.fillAmount = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (LoadingImage.fillAmount < 1)
        {
            LoadingImage.fillAmount += SplashCompleteSpeed * Time.deltaTime;
        }
        else
        {
            LoadingImage.fillAmount = 1;
        }
	


	}
}
